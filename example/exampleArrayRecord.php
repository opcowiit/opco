<?php

use ttlt\fly\helper\ArrayRecord;

include __DIR__ . '/../src/helper/ArrayRecord.php';

$records = [
    [
        'id' => 1,
        'name' => "test",
        'tel' => "123456",
        'type' => 0,
    ],
    [
        'id' => 2,
        'name' => "test",
        'tel' => "123456",
        'type' => 1,
    ],
    [
        'id' => 3,
        'name' => "test",
        'tel' => "123456",
        'type' => 3,
    ],
];
//testReplaceColumn();
//testCopyColumn();
//testDeleteColumns();
//testPackageColumns();
//testMapColumn();
//testCreateNew();
//testMap();
//testJoin();

function testReplaceColumn()
{
    global $records;
    $res = ArrayRecord::replaceColumn($records, 'type', [0 => 'type0', 1 => 'type1', 2 => 'type2']);
    var_export($res);
}

function testCopyColumn()
{
    global $records;
    $res = ArrayRecord::copyColumn($records, 'type', 'type1', true);
    var_export($res);
}

function testDeleteColumns()
{
    global $records;
    $res = ArrayRecord::deleteColumns($records, ['id', 'tel']);
    var_export($res);
}

function testPackageColumns()
{
    global $records;
    $res = ArrayRecord::packageColumns($records, 'test', 'id,tel');
    var_export($res);
}

function testMapColumn()
{
    global $records;
    $res = ArrayRecord::mapColumn($records, 'tel', function ($column) {
        return date('Y-m-d H:i:s', $column);
    });
    var_export($res);
}

function testCreateNew()
{
    global $records;
    $res = ArrayRecord::createNew($records, ['id->id1', 'name', 'tel->tel1', 'type1->type2']);
    var_export($res);
}

function testMap()
{
    global $records;
    $res = ArrayRecord::map($records, function ($record) {
        unset($record['id']);
        $record['abc'] = 1;
        return $record;
    });
    var_export($res);
}

function testJoin()
{
    global $records;
    $records1 = [
        ['name' => '类型1', 'id' => 0],
        ['name' => '类型2', 'id' => 2],
        ['name' => '类型2', 'id' => 3],
    ];
    $res = ArrayRecord::join($records, $records1, 'type', 'id','t123');
    var_export($res);
}