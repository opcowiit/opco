<?php
use ttlt\fly\helper\TreeRecord;

include __DIR__ . '/../src/helper/TreeRecord.php';

$records = [
    ['id'=>1,'pid'=>0,'name'=>'name1'],
    ['id'=>2,'pid'=>0,'name'=>'name2'],
    ['id'=>3,'pid'=>0,'name'=>'name3'],
    ['id'=>4,'pid'=>1,'name'=>'name4'],
    ['id'=>5,'pid'=>2,'name'=>'name5'],
    ['id'=>6,'pid'=>3,'name'=>'name6'],
    ['id'=>7,'pid'=>3,'name'=>'name7'],
    ['id'=>8,'pid'=>2,'name'=>'name8'],
    ['id'=>9,'pid'=>1,'name'=>'name9'],
    ['id'=>10,'pid'=>7,'name'=>'name10'],
    ['id'=>11,'pid'=>8,'name'=>'name11'],
    ['id'=>12,'pid'=>9,'name'=>'name12'],
    ['id'=>13,'pid'=>12,'name'=>'name13'],
    ['id'=>14,'pid'=>13,'name'=>'name14'],
    ['id'=>15,'pid'=>14,'name'=>'name15'],
];

//testFindChildRecords();
//testFindParentRecords();
//testFormatToTree();
//testRepair();

function testFindChildRecords(){
    global $records;
    $TreeRecord = new TreeRecord($records,'id','pid');
    $res = $TreeRecord->findChildRcords(1);
    var_export($res);
}

function testFindParentRecords(){
    global $records;
    $TreeRecord = new TreeRecord($records,'id','pid');
    $res = $TreeRecord->findParentRecords(15);
    var_export($res);
}

function testFormatToTree(){
    global $records;
    $TreeRecord = new TreeRecord($records,'id','pid');
    $res = $TreeRecord->formatToTree(0);
    echo json_encode($res,JSON_PRETTY_PRINT);
}

function testRepair(){
    $records = [
        ['id'=>1,'pid'=>0,'name'=>'name1'],
        ['id'=>2,'pid'=>5,'name'=>'name2'],
        ['id'=>3,'pid'=>2,'name'=>'name3']
    ];
    $TreeRecord = new TreeRecord($records,'id','pid');
    $res = $TreeRecord->repair(0)->formatToTree(0);
    var_export($res);
}