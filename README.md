#### 安装
```
composer require ttlt/fly
```
#### 说明

##### 生成thinkphp框架model类注释
```
$GenerateTpModelNotes = new GenerateTpModelNotes([
    'database'=>'test', //数据库名称
    'prefix'=>'', //表前缀
    'model_dir'=>'', //模型目录
    'namespace'=>'' //模型的命名空间
]);
$GenerateTpModelNotes->run();
```
##### 生成thinkphp框架通用参数类
```
$GenerateStructParam = new GenerateStructParam([
    'database'=>'test', //数据库名称
    'prefix'=>'', //表前缀
    'struct_dir'=>'', //模型目录
    'namespace'=>'' //模型的命名空间
]);
$GenerateStructParam->run();
```
##### 二维数组通用处理类
```
$records = [
    [
        'id' => 1,
        'name' => "test",
        'tel' => "123456",
        'type' => 0,
    ],
    [
        'id' => 2,
        'name' => "test",
        'tel' => "123456",
        'type' => 1,
    ],
    [
        'id' => 3,
        'name' => "test",
        'tel' => "123456",
        'type' => 3,
    ],
];

//替换二维数组的一列的数据
$replacement = [0 => 'type0', 1 => 'type1', 2 => 'type2'];
$res = ArrayRecord::replaceColumn($records, 'type', $replacement);

//复制二维数组的一列
$res = ArrayRecord::copyColumn($records, 'type', 'type1', true);

//删除二维数组一些列
$res = ArrayRecord::deleteColumns($records, ['id', 'tel']);

//打包二维数组的一些列为新的一列
$res = ArrayRecord::packageColumns($records, 'test', 'id,tel');

//为二维数组的一列应用回调函数
$res = ArrayRecord::mapColumn($records, 'tel', function ($column) {
    return date('Y-m-d H:i:s', $column);
});

//为二维数组应用回调
$res = ArrayRecord::map($records, function ($record) {
    unset($record['id']);
    $record['abc'] = 1;
    return $record;
});

//一个二维数组连接另一个二维数组
$records1 = [
    ['name' => '类型1', 'id' => 0],
    ['name' => '类型2', 'id' => 2],
    ['name' => '类型2', 'id' => 3],
];
$res = ArrayRecord::join($records, $records1, 'type', 'id','t123');
```

##### 二维数组通用处理类

```
$records = [
    ['id'=>1,'pid'=>0,'name'=>'name1'],
    ['id'=>2,'pid'=>0,'name'=>'name2'],
    ['id'=>3,'pid'=>0,'name'=>'name3'],
    ['id'=>4,'pid'=>1,'name'=>'name4'],
    ['id'=>5,'pid'=>2,'name'=>'name5'],
    ['id'=>6,'pid'=>3,'name'=>'name6'],
    ['id'=>7,'pid'=>3,'name'=>'name7'],
    ['id'=>8,'pid'=>2,'name'=>'name8'],
    ['id'=>9,'pid'=>1,'name'=>'name9'],
    ['id'=>10,'pid'=>7,'name'=>'name10'],
    ['id'=>11,'pid'=>8,'name'=>'name11'],
    ['id'=>12,'pid'=>9,'name'=>'name12'],
    ['id'=>13,'pid'=>12,'name'=>'name13'],
    ['id'=>14,'pid'=>13,'name'=>'name14'],
    ['id'=>15,'pid'=>14,'name'=>'name15'],
];
$TreeRecord = new TreeRecord($records,'id','pid');

//查找下级记录
$res = $TreeRecord->findChildRcords(1);

//查找父记录
$res = $TreeRecord->findParentRecords(15);

//转成树形结构
$res = $TreeRecord->formatToTree(0);

//修复上下级断层
$records = [
    ['id'=>1,'pid'=>0,'name'=>'name1'],
    ['id'=>2,'pid'=>5,'name'=>'name2'],
    ['id'=>3,'pid'=>2,'name'=>'name3']
];
$TreeRecord = new TreeRecord($records,'id','pid');
$res = $TreeRecord->repair(0)->formatToTree(0);
```

##### 服务进程管理类
```
/**
 * 带守护进程启动命令: php exampleProcessManager.php daemon
 * 不带守护进程启动命令: php exampleProcessManager.php
 * 不带守护进程启动命令: php exampleProcessManager.php start
 * 重启服务进程命令：php exampleProcessManager.php restart
 * 停止命令：php exampleProcessManager.php stop
 */
 
//简单使用
ServiceProcessManager::setup()->setDebug(true)->dispatch();

//下面是业务逻辑例子
while (1) {
    //获取进程pid
    if (function_exists('posix_getpid')) {
        $pid = posix_getpid();
    } else {
        $pid = getmypid();
    }
    //接收父进程命令
    ServiceProcessManager::getInstance()->recvProcessCmd();
    global $argv;
    echo "[childProcess] [pid=" . $pid . '] [command=php ' . implode(' ',$argv) .']'. PHP_EOL;
    sleep(1);
}
```

```
//高级使用
ServiceProcessManager::setup([
    ProcessDescStruct::create('', 'actionOne', 1),
    ProcessDescStruct::create('', 'actionTwo', 1)
])
->setDebug(true)
->setOsUser('www')
->setDaemonProcessName('ProcessName')
->setDaemonLoopInterval(1000)
->dispatch();
```


```
//灵活使用
ServiceProcessManager::setup(ProcessDescStruct::create('','actionOne',1));
ServiceProcessManager::setup(ProcessDescStruct::create('','actionTwo',1));
$manager = ServiceProcessManager::getInstance();
$manager->setDebug(true);
$manager->dispatch();
```