<?php
namespace ttlt\fly\helper;

/**
 * ArrayRecord
 * 二维数组通用处理类
 */
class ArrayRecord
{
    /**
     * 替换二维数组的一列的数据
     *
     * @param array $records 二维数组
     * @param string $column 要替换的字段名称
     * @param array $replacement 对应的替换数据
     * @return array
     */
    public static function replaceColumn($records, $column, $replacement)
    {
        foreach ($records as $k => $record) {
            if (isset($replacement[$record[$column]])) {
                $records[$k][$column] = $replacement[$record[$column]];
            }
        }
        return $records;
    }
    /**
     * 复制二维数组的一列
     *
     * @param array $records 二维数组
     * @param string $sourceColumn 来源列字段名称
     * @param string $distColumn 复制后的字段名称
     * @param boolean $retainSourceColumn 默认false,是否保留原列
     * @return array
     */
    public static function copyColumn($records, $sourceColumn, $distColumn, $retainSourceColumn = false)
    {
        foreach ($records as $k => $record) {
            if (isset($record[$sourceColumn])) {
                $records[$k][$distColumn] = $records[$k][$sourceColumn];
                if (!$retainSourceColumn) {
                    unset($records[$k][$sourceColumn]);
                }
            } else {
                $records[$k][$distColumn] = null;
            }
        }
        return $records;
    }
    /**
     * 删除二维数组一些列
     *
     * @param array $records 二维数组
     * @param string|array $columns 要删除的列，字符串的多列用逗号隔开 
     * @return array
     */
    public static function deleteColumns($records, $columns)
    {
        if (is_string($columns)) {
            $columns = explode(',', $columns);
        }
        foreach ($records as $k => $record) {
            foreach ($columns as $column) {
                if (isset($record[$column])) {
                    unset($records[$k][$column]);
                }
            }
        }
        return $records;
    }
    /**
     * 打包二维数组的一些列为新的一列
     *
     * @param array $records 二维数组
     * @param string $newColumn 新列名称
     * @param string|array $columns 要删除的列，字符串的多列用逗号隔开 
     * @return array
     */
    public static function packageColumns($records, $newColumn, $columns)
    {
        if (is_string($columns)) {
            $columns = explode(',', $columns);
        }
        foreach ($records as $k => $record) {
            $arr = [];
            foreach ($columns as $column) {
                if (isset($record[$column])) {
                    $arr[$column] = $record[$column];
                    unset($records[$k][$column]);
                } else {
                    $arr[$column] = null;
                }
            }
            $records[$k][$newColumn] = $arr;
        }
        return $records;
    }
    /**
     * 为二维数组的一列应用回调函数
     *
     * @param array $records 二维数组
     * @param string $column 列名称
     * @param callable $callable 回调
     * @return array
     */
    public static function mapColumn($records, $column, $callable)
    {
        foreach ($records as $k => $record) {
            $records[$k][$column] = $callable($record[$column]);
        }
        return $records;
    }
    /**
     * 创建一个新的二维数组
     * 不推荐用这个方法了，用原生的语法跟适合
     * @param array $records 二维数组
     * @param string|array $renames 要有的字段，和新字段规则；规则：名称->新名称;名称
     * @return array
     */
    public static function createNew($records, $renames)
    {
        if (is_string($renames)) {
            $renames = preg_replace('/\s/', '', $renames);
            $renames = explode(',', $renames);
        }
        foreach ($renames as $k => $rename) {
            $rename = explode('->', $rename);
            if (count($rename) == 1) {
                $rename[1] = $rename[0];
            }
            $renames[$k] = $rename;
        }
        $newRecords = [];
        foreach ($records as $record) {
            $newRecord = [];
            foreach ($renames as $rename) {
                if (isset($record[$rename[0]])) {
                    $newRecord[$rename[1]] = $record[$rename[0]];
                } else {
                    $newRecord[$rename[1]] = null;
                }
            }
            $newRecords[] = $newRecord;
        }
        return $newRecords;
    }
    /**
     * 为二维数组应用回调
     *
     * @param array $records 二维数组
     * @param callable $callable 回调
     * @return array
     */
    public static function map($records, $callable)
    {
        foreach ($records as $k => $record) {
            $records[$k] = $callable($record);
        }
        return $records;
    }
    /**
     * 一个二维数组连接另一个二维数组
     *
     * @param array $records 二维数组
     * @param array $records1 另一个二维数组
     * @param string $foreignKey 第一个二维数组的key
     * @param string $key1 另一个二维数组的key
     * @param string $newColumn 新字段名称，默认为空，直接用另一个二维数组的字段
     * @param integer $merge 新字段名称为空有用，合并的方向，默认-1，
     * @return array
     */
    public static function join($records, $records1, $foreignKey, $key1, $newColumn = '',$merge=-1)
    {
        $tmp = [];
        foreach ($records1 as $record1) {
            $tmp[$record1[$key1]] = $record1;
        }
        if( $newColumn == '' ){
            if( isset($records1[0]) ){
                $null = $records1[0];
                foreach( $null as $k=>$v ){
                    $null[$k] = null;
                }
            }else{
                $null = [];
            }
        }
        //var_export($null);
        foreach ($records as $k => $record) {
            if( $newColumn != '' ){
                if (isset($tmp[$record[$foreignKey]])) {
                    $records[$k][$newColumn] = $tmp[$record[$foreignKey]];
                } else {
                    $records[$k][$newColumn] = [];
                }
            }else{
                if( $merge == -1 ){
                    if (isset($tmp[$record[$foreignKey]])) {
                        $records[$k] = array_merge($tmp[$record[$foreignKey]],$records[$k]);
                    } else {
                        $records[$k] = array_merge($null,$records[$k]);
                    }
                }else{
                    if (isset($tmp[$record[$foreignKey]])) {
                        $records[$k] = array_merge($records[$k],$tmp[$record[$foreignKey]]);
                    } else {
                        $records[$k] = array_merge($records[$k],$null);
                    }
                }
            }
        }
        return $records;
    }
}