<?php

namespace ttlt\fly\process;

/**
 * class ServiceProcessManager
 * 
 * 进程管理、守护进程
 */
class ServiceProcessManager
{
    //@var boolean $debug 调试模式；默认是false，不启用
    private $debug = false;

    //@var string $osUser 系统用户
    private $osUser = '';

    //@var callable $daemonLoopCb 守护进程循环过程中回调,用户自定义业务
    private $daemonLoopCb = null;

    //@var string $debugInfoFile 守护进程日志信息文件
    private $debugInfoFile = __DIR__ . '/debugInfo.log';

    //@var string $daemonProcessName 守护进程名称
    private $daemonProcessName = 'php service manager';
    
    //@var string $processCmdFile 进程通信文件
    private $processCmdFile = __DIR__ . '/process.cmd';

    //@var integer $daemonLoopInterval 守护进程循环间隔，单位毫秒
    private $daemonLoopInterval = 1000;

    //@var array $processDesc 进程的描述信息数组
    private $processDescs = [];

    //@var array $instance 实例
    private static $instance;

    public static function getInstance(){
        if( self::$instance == null ){
            self::$instance = new self;
        }
        return self::$instance;
    }

    public static function setup($processDescs = null){

        $self = self::getInstance();

        //安装进程描述符
        if( $processDescs == null ){
            $self->processDescs[] = ProcessDescStruct::create();
        }elseif( $processDescs instanceof ProcessDescStruct ){
            $self->processDescs[] = $processDescs;
        }elseif( is_array($processDescs) ){
            foreach ($processDescs as $k => $v) {
                if ( $v instanceof ProcessDescStruct) {
                    $self->processDescs[] = $v;
                }
            }
        }
        return $self;
    }

    public function setDebug( $debug ){
        $this->debug = $debug;
        return $this; 
    }

    public function setOsUser($osUser){
        $this->osUser = $osUser;
        return $this;
    }

    public function setDaemonLoopCb($daemonLoopCb){
        $this->daemonLoopCb = $daemonLoopCb;
        return $this;
    }

    public function setDebugInfoFile($debugInfoFile){
        $this->debugInfoFile = $debugInfoFile;
        return $this;
    }

    public function setDaemonProcessName($daemonProcessName){
        $this->daemonProcessName = $daemonProcessName;
        return $this;
    }

    public function setProcessCmdFile($processCmdFile){
        $this->processCmdFile = $processCmdFile;
        return $this;
    }

    public function setDaemonLoopInterval($daemonLoopInterval){
        $this->daemonLoopInterval = $daemonLoopInterval;
        return $this;
    }
    
    /**
     * 根据命令来调度
     */
    public function dispatch()
    {
        
        //命令行参数分支，获取在终端命令行的最后一个参数
        global $argv;
        $type = end($argv);
        switch ($type) {
            case 'start':
                return;
            case 'restart':
                file_put_contents($this->processCmdFile, 'restart');
                exit;
            case 'stop':
                file_put_contents($this->processCmdFile, 'exit');
                exit;
            case 'daemon':
                break;
            case 'posix_setuid':
                break;
            default:
                return;
        }
        
        $processCmdFile = $this->processCmdFile;
        $osUser = $this->osUser;
        $daemonProcessName = $this->daemonProcessName;
        $daemonLoopInterval = $this->daemonLoopInterval;
        $daemonLoopCb = $this->daemonLoopCb;

        //判断程序是否运行
        if (is_file($processCmdFile)) {
            $res = @unlink($processCmdFile);
            //var_dump($res);exit;
            if (!$res) {
                exit('program is running');
            }
        }

        //守护进程可以启动的
        file_put_contents($processCmdFile, '');
        $this->debugInfo("Daemon process is ok");

        //是否是windows系统
        $isWindows = (strtoupper(substr(PHP_OS, 0, 3)) == 'WIN');

        //pipe描述符
        if ($isWindows) {
            $descriptorspec = [];
        } else {
            $descriptorspec = [
                0 => array('pipe', 'a'), // 标准输入，子进程从此管道中读取数据
                1 => array('file', '/dev/null', 'w'), // 标准输出，子进程向此管道中写入数据
                2 => array('file', '/dev/null', 'w'), // 标准错误，写入到一个文件
            ];
        }

        //记录用户自定义进程用户
        if ($osUser != '') {
            $this->debugInfo("Set (\$osUser=" . $osUser . ")");
        }

        //记录用户自定义守护进程名称
        if ($daemonProcessName != '' && $daemonProcessName != 'php service manager') {
            $this->debugInfo("Set (\$daemonProcessName=" . $daemonProcessName . ")");
        }

        //给进程设置用户
        if (!$isWindows && $osUser != '') {
            $user_info = posix_getpwnam($osUser);
            if (!$user_info) {
                $this->debugInfo('User {$osUser} not exsits', 'warning');
                exit;
            }
            $uid = $user_info['uid'];
            $gid = $user_info['gid'];
            if ($uid != posix_getuid() || $gid != posix_getgid()) {
                if (!posix_setgid($gid) || !posix_initgroups($user_info['name'], $gid) || !posix_setuid($uid)) {
                    $this->debugInfo('Change gid or uid fail', 'warning');
                    exit;
                }
            }
        }

        //启动子进程命令
        $arr = array_slice($argv, 0, count($argv) - 1);
        $command = 'php ' . implode(' ', $arr);
        //var_export($command);exit;

        //启动新的进程，并退出
        if (!$isWindows && $type == 'daemon') {
            //echo $command . ' posix_setuid' . '启动' . PHP_EOL;
            $descriptorspec = [];
            proc_open($command . ' posix_setuid', $descriptorspec, $pipes, null, null);
            exit;
        }

        //使当前进程成为当前会话主进程
        if (!$isWindows) {
            fclose(STDIN);
            posix_setsid();
        }

        //启动子进程
        //echo $command . '启动' . PHP_EOL;
        //@var ProcessDescStruct $v
        $processDescs = $this->startChildProcess($this->processDescs);

        //设置守护进程名称
        if ($daemonProcessName != '') {
            cli_set_process_title($daemonProcessName);
            $this->debugInfo('Set daemon process title is (' . $daemonProcessName . ')');
        }

        //打开进程通信命令文件
        $fd = fopen($processCmdFile, 'r');

        //运行
        $count = 0;
        if ($daemonLoopInterval <= 0) {
            $daemonLoopInterval = 1000;
        }
        $recordMemoryCount = (int) (12 * 3600 * 1000 / $daemonLoopInterval);

        while (1) {

            //查看命令
            rewind($fd);
            $cmd = fgets($fd);
            //var_export($cmd) . PHP_EOL;
            if (trim($cmd) == 'exit') {
                file_put_contents($processCmdFile, 'childProcessExit');
                break;
            } elseif (trim($cmd) == 'restart') {
                file_put_contents($processCmdFile, 'childProcessExit');
                $this->closeChildProcess($processDescs);
                file_put_contents($processCmdFile, '');
                $processDescs = $this->startChildProcess($processDescs);
            }

            //守护子进程状态
            foreach ($processDescs as $k => $v) {
                foreach ($v->procs as $i => $proc) {
                    //var_dump($proc);
                    if ($proc == false) {
                        $this->debugInfo('Child process exception exit (' . $v->command . ')', 'warning');
                        $processDescs[$k]->procs[$i] = proc_open($v->command, $v->descriptorspec, $v->pipes, $v->cwd, $v->env_vars, $v->options);
                        break;
                    }
                    $status = proc_get_status($proc);
                    if ($status == false || $status['running'] == false) {
                        $this->debugInfo('Child process exception exit (' . $v->command . ')', 'warning');
                        proc_close($proc);
                        $processDescs[$k]->procs[$i] = proc_open($v->command, $v->descriptorspec, $v->pipes, $v->cwd, $v->env_vars, $v->options);
                    }
                }
            }

            //记录守护进程内存情况
            $count++;
            if ($count >= 3600) {
                $count = 0;

            }

            $this->recordUseMemorySize($recordMemoryCount);

            if (is_callable($daemonLoopCb)) {
                call_user_func($daemonLoopCb, [$command, $processDescs]);
            }
            //var_dump($daemonLoopInterval);exit;
            usleep($daemonLoopInterval * 1000);
        }

        //回收子进程资源
        $this->closeChildProcess($processDescs);

        fclose($fd);

        //删除进程通信命令文件
        unlink($processCmdFile);
        $this->debugInfo('Daemon process exit');
        exit;
    }

    private function closeChildProcess($processDescs)
    {
        $procs = [];
        foreach ($processDescs as $k => $v) {
            $procs = array_merge($procs, $v->procs);
        }
        do {
            foreach ($procs as $k => $proc) {
                $status = proc_get_status($proc);
                if ($status['running'] == false) {
                    proc_close($proc);
                    unset($procs[$k]);
                }
            }
            usleep(100);
        } while (count($procs) > 0);
    }

    private function startChildProcess($processDescs)
    {
        foreach ($processDescs as $k => $v) {
            //var_export($v->command);exit;
            $v->procs = [];
            for ($i = 0; $i < $v->count; $i++) {
                $v->procs[] = proc_open($v->command, $v->descriptorspec, $v->pipes, $v->cwd, $v->env_vars, $v->options);
                $this->debugInfo('Start child process; command is (' . $v->command . ')');
            }
            $processDescs[$k] = $v;
        }
        return $processDescs;
    }
    /**
     * 子进程接收命令，建议该方法每间隔1秒钟执行一次
     *
     * @param callable $cb 子进程退出时要执行的回调方法
     * @return void
     */
    public function recvProcessCmd($cb = null)
    {
        //获取命令
        static $fd;
        
        $processCmdFile = $this->processCmdFile;
        if (!is_file($processCmdFile)) {
            touch($processCmdFile);
        }
        if ($fd === null) {
            $fd = fopen($processCmdFile, 'r');
            stream_set_blocking($fd, false);
        }
        rewind($fd);
        $cmd = fgets($fd);
        //echo 'recv cmd:' . $cmd . PHP_EOL;

        //执行命令，执行回调
        if (trim($cmd) == 'childProcessExit') {
            //echo 'recv cmd:' . $cmd . PHP_EOL;
            fclose($fd);
            if (is_callable($cb)) {
                call_user_func($cb);
            }
            //$this->debugInfo('Child process exit', 'info', 'childProcess');
            exit;
        }
    }

    /**
     * 打印和记录调试信息
     *
     * @param string $msg 消息
     * @param string $level 消息等级
     * @param string $processType 进程类型，默认是daemonProcess
     * @return void
     */
    public function debugInfo($msg, $level = 'info', $processType = 'daemonProcess')
    {
        //获取进程pid
        if (function_exists('posix_getpid')) {
            $pid = posix_getpid();
        } else {
            $pid = getmypid();
        }

        //拼接消息
        $msg = '[' . $processType . '] [pid=' . $pid . '] [' . $level . '] ' . $msg;

        //打印消息
        if ($this->debug) {
            if ($level != 'info') {
                fwrite(STDERR, $msg . PHP_EOL);
            } else {
                echo $msg . PHP_EOL;
            }
        }

        //记录消息
        file_put_contents($this->debugInfoFile, '[' . date('c') . '] ' . $msg . PHP_EOL, FILE_APPEND);
    }

    /**
     * 记录内存使用信息
     *
     * @param integer $intervalCount 运行多少次记录一次
     * @param string $processType 进程类型，默认是daemonProcess
     * @return void
     */
    public function recordUseMemorySize($intervalCount = 1000, $processType = 'daemonProcess')
    {
        //计数
        static $count;
        if ($count === null) {
            $count = $intervalCount;
        }
        $count++;
        if ($count < $intervalCount) {
            return;
        }
        $count = 0;

        //内存大小计算
        $size = round(memory_get_usage() / 1024, 2) . 'KB';

        //记录
        $this->debugInfo('Has used memory size (' . $size . ')', 'info', $processType);

    }
}